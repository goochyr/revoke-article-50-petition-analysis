import urllib.request, json, csv, io, datetime, math

url1 = 'http://researchbriefings.files.parliament.uk/documents/CBP-7979/HoC-GE2017-constituency-results.csv'

#with open('GE2017.csv') as csvfile:
with urllib.request.urlopen(url1) as urlfile:
    csvfile = io.TextIOWrapper(urlfile, encoding = 'utf-8')
    const_data = csv.DictReader(csvfile)
    data_dict = {}
    for row in const_data:
        votes = [row['con'], row['lab'], row['ld'], row['ukip'], row['green'], row['snp'], row['pc'], row['dup'], row['sf'], row['sdlp'], row['uup'], row['alliance'], row['other']]
        for i in range(len(votes)):
            votes[i] = int(votes[i])
        winner_vote = max(votes)
        data_dict[row['ons_id']] = {'name':row['constituency_name'], 'electorate':int(row['electorate']), 'majority':int(row['majority']), 'winning_vote': winner_vote}

url2 = 'https://petition.parliament.uk/petitions/241584.json'

with urllib.request.urlopen(url2) as datafile:
    petition_data = json.loads(datafile.read().decode())

constituencies = petition_data['data']['attributes']['signatures_by_constituency']

for item in constituencies:
    data_dict[item['ons_code']]['sig_count'] = int(item['signature_count'])
    data_dict[item['ons_code']]['mp'] = item['mp']

list_winner = []
list_majority = []

for k in data_dict:
    percentage = '{:0.2f}'.format(100*data_dict[k]['sig_count']/data_dict[k]['electorate'])
    if data_dict[k]['sig_count'] >= data_dict[k]['winning_vote']:
        list_winner.append((data_dict[k]['mp'], data_dict[k]['sig_count'], data_dict[k]['electorate'], percentage, data_dict[k]['winning_vote'], data_dict[k]['sig_count'] - data_dict[k]['winning_vote']))
    elif data_dict[k]['sig_count'] >= data_dict[k]['majority']:
        list_majority.append((data_dict[k]['mp'], data_dict[k]['sig_count'], data_dict[k]['electorate'], percentage, data_dict[k]['winning_vote'], data_dict[k]['majority'], data_dict[k]['sig_count'] - data_dict[k]['majority']))

list_winner.sort(key=lambda x: x[5], reverse=True)
list_majority.sort(key=lambda x: x[6], reverse=True)

textlines = []
textlines.append('# Analysis of the petition \'Revoke Article 50 and remain in the EU.\' (Petition 241584) \n')
textlines.append('*Information correct as of %s*\n' % datetime.datetime.now())
textlines.append('## List of MPs for which more of their constituents signed the Revoke Article 50 petition than voted for them in the 2017 General election (Sorted by difference)\n\n\n')
textlines.append('|MP|Number of constituents who signed petition|Electorate|Percentage|Number who voted for the winner in 2017|Difference|\n')
textlines.append('|--|--|--|--|--|--|\n')
for constituency in list_winner:
    textlines.append('|%s|%s|%s|%s|%s|%s|\n' % constituency)

textlines.append('## List of MPs for which more of their constituents signed the Revoke Article 50 petition than the 2017 Majority (Sorted by difference)\n\n\n')
textlines.append('|MP|Number of constituents who signed petition|Electorate|Percentage|2017 Votes|2017 Majority|Difference|\n')
textlines.append('|--|--|--|--|--|--|--|\n')
for constituency in list_majority:
    textlines.append('|%s|%s|%s|%s|%s|%s|%s|\n' % constituency)

with open('latestresults.md', 'w') as writefile:
    for line in textlines:
        writefile.write(line)

list_print = []

for i in list_majority:
    list_print.append((i[0],i[1] - i[4]))

list_print.sort(key=lambda x: x[1], reverse=True)
print(list_print[:9])